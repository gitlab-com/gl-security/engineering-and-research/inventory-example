# GitLab Inventory example

This project is a public demo of how to build an inventory of GitLab projects using the [GitLab Inventory Builder](https://gitlab.com/gitlab-com/gl-security/product-security/gib).

The [data](./data) folder is used as configuration and storage. It is currently in an initial stage, where nothing has been synced yet. For this example, we created the following structure:

```
.
└── data
    └── gitlab-org
        ├── 5-minute-production-app
        │   └── .gitkeep
        ├── customers-gitlab-com
        │   └── properties.yml
        ├── gitlab-services
        │   ├─── design.gitlab.com
        │   │    └── properties.yml
        │   └── ignore
        └── ignore
```

Which can be read as:

```
.
└── data
    └── gitlab-org <== Our groups/projects are under https://gitlab.com/gitlab-org
        ├── 5-minute-production-app <== This subgroup will be synced
        │   └── .gitkeep <== This file is allowed, and necessary to keep the empty directory until the sync
        ├── customers-gitlab-com <== This standalone project will be synced.
        │   └── properties.yml <== This file defines the categories and urls for this project
        ├── gitlab-services <== This group will be just traversed
        │   ├── design.gitlab.com <== This standalone project will be synced
        │   │   └── properties.yml <== This file defines the categories and urls for this project
        │   └── ignore <== gitlab-services won't be synced
        └── ignore <== gitlab-org won't be synced
```

In this example, we want to synchronise subgroups and projects of [https://gitlab.com/gitlab-org/5-minute-production-app/](https://gitlab.com/gitlab-org/5-minute-production-app/),
and two [standalone projects](https://gitlab.com/gitlab-com/gl-security/product-security/gib/#standalone-projects).

After running the project pipeline, a job like [this one](https://gitlab.com/gitlab-com/gl-security/product-security/inventory-example/-/jobs/1652783871) will sync the `data` folder with the data fetched from the GitLab API. A [merge request is created](https://gitlab.com/gitlab-com/gl-security/product-security/inventory-example/-/jobs/1652783890) to [review and merge the changes](https://gitlab.com/gitlab-com/gl-security/product-security/inventory-example/-/merge_requests/10). If merged, the `data` folder will look like this:

```
.
└── data
    └── gitlab-org
        ├── 5-minute-production-app
        │   ├── deploy-template
        │   │   └── project.json
        │   ├── examples
        │   │   ├── clojure-web-application
        │   │   │   └── project.json
        │   │   ├── django-in-five
        │   │   │   └── project.json
        │   │   ├── expressjs-app
        │   │   │   └── project.json
        │   │   ├── rails-app
        │   │   │   └── project.json
        │   │   ├── rails6-app
        │   │   │   └── project.json
        │   │   ├── spring-app
        │   │   │   └── project.json
        │   │   └── group.json
        │   ├── hipio
        │   │   └── project.json
        │   ├── sandbox
        │   │   ├── cats
        │   │   │   └── project.json
        │   │   ├── rails-6-sqlite
        │   │   │   └── project.json
        │   │   ├── rails-demo-2021-01-21
        │   │   │   └── project.json
        │   │   ├── rails-test-1
        │   │   │   └── project.json
        │   │   ├── rails-test-2
        │   │   │   └── project.json
        │   │   ├── rails-test-3
        │   │   │   └── project.json
        │   │   ├── test-darby-credentials
        │   │   │   └── project.json
        │   │   └── group.json
        │   ├── static-template
        │   │   └── project.json
        │   └── .gitkeep
        ├── customers-gitlab-com
        │   └── properties.yml
        ├── gitlab-services
        │   ├── design.gitlab.com
        │   │   ├── project.json
        │   │   └── properties.yml
        │   └── ignore
        └── ignore
```

## Configuration

To build the inventory, the [Inventory Builder include file](https://gitlab.com/gitlab-com/gl-security/product-security/gib/-/blob/main/ci/Inventory-Builder.gitlab-ci.yml)
is included in the [`.gitlab-ci.yml`](./.gitlab-ci-.yml) file of this repository.
Checkout this file for details on variables to configure the job.

### Authentication

Group Access Token are used to synchronize each top-level namespace. These tokens are configured via
`sync_token_name` files in the `data` folder. See
https://gitlab.com/gitlab-com/gl-security/product-security/gib#use-multiple-sync-tokens for more
details. The value of these variables is set in the settings of the project, as a
[protected](https://docs.gitlab.com/ee/ci/variables/#protected-variables) and
[masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) variable.

A [Project Access
Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) is used to
commit changes and create issues (violations based on our policies). This part uses Basic HTTP
authentication, hence the `GITLAB_USERNAME` variable. `GITLAB_PASSWORD` is set in the settings of
the project, as a [protected](https://docs.gitlab.com/ee/ci/variables/#protected-variables) and
[masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) variable.
